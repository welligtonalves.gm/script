#!/bin/bash
#########################################################################
#         Script criado por Welligton Alves Analista eulabs             #
#     Lista endereços de ips com tentavias de login unknown e envia     #
#     Para uma lista de bloqueio utilizando mikrotik como fireewall     #
#########################################################################

#Dados para conexao com a mikrotik

HOST=10.2.3.1     #ip routerboard
PORT=22           #porta de acesso ssh
USER=zimbra       #usuario de acesso mk
PASS=123          #senha de acesso mk

log=/var/log/log-fw.txt

ip_list=/root/ip_list
IP_LIST=/root/ipok.list

echo "Inicio: `date +%d-%m-%y_%H:%M:%S`" >> $log

cat /var/log/maillog|grep warning:|grep unknown | grep -v 192.168.10.61 | grep -Po '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+' | sort| uniq > ip_list

echo "vefiricando enderecos duplicados em lista_ip"

cat $ip_list  | sort | uniq  > $IP_LIST

for IP in $(cat $IP_LIST);
  do
    sshpass -p $PASS  ssh $USER@$HOST -p $PORT /ip firewall address-list add address=$IP list=DDoS_Zimbra;
    echo $IP
  done


awk 'END{print NR}' $LIST_BKP >> $log
echo "novos enderecos foram adicionados na lista de bloqueio! $(date +%F\ %T)" >> $log

echo "Fim: `date +%d-%m-%y_%H:%M:%S`" >> $log

exit

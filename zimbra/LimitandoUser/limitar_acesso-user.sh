#!/bin/bash
#################################################################
#       Script criado por Welligton Alves Analista eulabs       #
#         Limitando acesso ao painel de controle zimbra         #
#################################################################

echo 'Informe o dominio que sera gerenciado'
echo 'Options:'
echo 'meudominio.com.br'

echo '=====Informe o dominio:======='

read DOMAIN;

echo '==== Iforme o email do usuario:===='

read EMAIL;

echo 'Aguarde estamos validando as informações'

sleep 5

echo ' iniciando processo'

su - zimbra -c  "zmprov ma $EMAIL zimbraIsDelegatedAdminAccount TRUE"

echo 'Adiciona permissão ao console de administração e no menu Gerenciar:'

su - zimbra -c "zmprov ma $EMAIL +zimbraAdminConsoleUIComponents accountListView"

echo 'Libera permissão para gerenciar contas(apagar,criar,editar,alterar senha, listar contas):'

su - zimbra -c "zmprov grr domain $DOMAIN usr $EMAIL domainAdminRights"

echo 'Adiciona permissão para gerenciar listas de distribuição'

su - zimbra -c "zmprov ma $EMAIL +zimbraAdminConsoleUIComponents DLListView"
echo 
echo
echo '============ FIM ============'
echo
echo
echo 'Usuario' $EMAIL 'tem permisão para editar, excluir, altera e criar usuarios no dominio' $DOMAIN

exit
